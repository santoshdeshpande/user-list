import React, { Component } from 'react';
import './App.css';

import UserListContainer from './containers/UserListContainer';

// const users = [
//   {firstName: 'Santosh', lastName: 'Suresh', email: 'santoshdeshpande@gmail.com', id: 1, photoUrl:'https://randomuser.me/api/portraits/thumb/men/21.jpg'},
//   {firstName: 'Santosh', lastName: 'Suresh', email: 'santoshdeshpande@gmail.com', id: 1, photoUrl:'https://randomuser.me/api/portraits/thumb/men/21.jpg'},
//   {firstName: 'Santosh', lastName: 'Suresh', email: 'santoshdeshpande@gmail.com', id: 1, photoUrl:'https://randomuser.me/api/portraits/thumb/men/21.jpg'},
//   {firstName: 'Santosh', lastName: 'Suresh', email: 'santoshdeshpande@gmail.com', id: 1, photoUrl:'https://randomuser.me/api/portraits/thumb/men/21.jpg'},
//   {firstName: 'Santosh', lastName: 'Suresh', email: 'santoshdeshpande@gmail.com', id: 1, photoUrl:'https://randomuser.me/api/portraits/thumb/men/21.jpg'},
//   {firstName: 'Santosh', lastName: 'Suresh', email: 'santoshdeshpande@gmail.com', id: 1, photoUrl:'https://randomuser.me/api/portraits/thumb/men/21.jpg'},
// ]
class App extends Component {
  render() {
    return (
      <div>
        <UserListContainer />
      </div>
    );
  }
}

export default App;
