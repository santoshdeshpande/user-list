import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

import 'rxjs';

import {createStore, applyMiddleware} from 'redux';
import {rootEpic, rootReducer} from './modules';
import {Provider} from 'react-redux';

import {createEpicMiddleware} from 'redux-observable';

const epics = applyMiddleware(createEpicMiddleware(rootEpic));

const store = createStore(rootReducer, epics);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
