import React, {Component} from 'react';
import 'whatwg-fetch';
import UserList from '../components/UserList';
import LoadingIndicator from '../components/Loading';
import {bindActionCreators} from 'redux';
import {actions} from '../modules/User';
import {connect} from 'react-redux';

class UserListContainer extends Component {
  componentWillMount() {
    this.props.fetchUsers();
  }

  render() {
    const {users, isLoading} = this.props;
    const component = isLoading ? <LoadingIndicator /> : <UserList users={users}></UserList>;
    return component;
  }
}

const mapStateToProps = (state) => ({
  users: state.users.allUsers,
  isLoading: state.users.isLoading
})

const mapDispatchToProps = (dispatch) => (
  bindActionCreators(actions, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(UserListContainer);
