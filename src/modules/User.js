import {ajax} from 'rxjs/observable/dom/ajax';
import {Observable} from 'rxjs';

const FETCH_USERS = 'user/FETCH_USERS';
const FETCH_USERS_SUCCESS = 'user/FETCH_USERS_SUCCESS';
const FETCH_USERS_ERROR = 'user/FETCH_USERS_ERROR';


const fetchUsers = () => ({
  type: FETCH_USERS
});


const fetchUsersSuccess = users => ({
  type: FETCH_USERS_SUCCESS,
  users
});

export const actions = {
  fetchUsers,
  fetchUsersSuccess
}



const INITIAL_STATE = {allUsers: [], isLoading: false}

const reducer = (state=INITIAL_STATE, action) => {
  switch(action.type) {
    case FETCH_USERS_SUCCESS:
      const newState = {...state, allUsers:action.users, isLoading: false};
      return newState;
    case FETCH_USERS:
      return {...state, isLoading: true}
    default:
      return state;
  }
}

const mapResultsToRequiredData = (json) => {
  return json.results.map(user => ({
    firstName: user.name.first,
    lastName: user.name.last,
    email: user.email,
    photoUrl: user.picture.thumbnail,
    id: user.md5
  })).sort((user1, user2) => user1.lastName.localeCompare(user2.lastName));
}

export const fetchUserEpic = action$ =>
  action$.ofType(FETCH_USERS)
    .mergeMap(action =>
      ajax.getJSON('https://api.randomuser.me/?results=100&nat=us,gb')
        .map(mapResultsToRequiredData)
        .map(fetchUsersSuccess)
        .catch(error => Observable.of({
          type: FETCH_USERS_ERROR,
          payload: error.xhr.response,
          error: true
        }))
    )


export default reducer;
