import {combineReducers} from 'redux';
import {combineEpics} from 'redux-observable';

import  UserReducer, {fetchUserEpic} from './User';


export const rootReducer = combineReducers({
  users: UserReducer
});


export const rootEpic = combineEpics(
  fetchUserEpic
);
