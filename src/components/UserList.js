import React from 'react';
import UserItem from './UserItem';

const UserList = ({users}) => (
  <div className="container-fluid">
    {users.map(user => <UserItem key={user.email} {...user} />)}
  </div>
)


export default UserList;
