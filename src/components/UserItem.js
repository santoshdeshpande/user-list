import React from 'react';
import './UserItem.css';

const UserItem = ({id, firstName, lastName, email, photoUrl}) => (
  <div className="media">
    <div className="media-left">
      <a href="#">
        <img src={photoUrl} alt={firstName} className="media-object img-circle"/>
      </a>
    </div>
    <div className="media-body">
      <h4 className="media-heading rct-username">{lastName}, {firstName}</h4>
      <p>{email}</p>
    </div>
  </div>

);


export default UserItem;
